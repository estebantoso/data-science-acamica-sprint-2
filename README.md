<div align="center"><img src="./assets/acamica_title.png"></div>

# Recursos DATA SCIENCE - Sprint 2

Un lugar con bibliografía, material adicional y muchas cosas más para complementar el material del curso.

## Links útiles

### Splitting y Métricas

| Link | Descripción |
| --- | --- |
| [Train-Test Split for Evaluating Machine Learning Algorithms](https://machinelearningmastery.com/train-test-split-for-evaluating-machine-learning-algorithms) | _Uso de Train-Test Split para diferentes problemas de ML (clasificación y regresión). Incluye información sobre estratificación._ |
| [The Lift Curve: Unveiled](https://towardsdatascience.com/the-lift-curve-unveiled-998851147871) | _Descripción sobre la curva Lift y su uso en proyectos de ML._ |

### Support Vector Machines (SVMs)

| Link | Descripción |
| --- | --- |
| [CS229 Lecture notes - Part V: SVMs](https://see.stanford.edu/materials/aimlcs229/cs229-notes3.pdf) | _Notas de Andrew Ng sobre los conceptos de SVMs._ |
| [Support Vector Machine — Introduction to Machine Learning Algorithms](https://towardsdatascience.com/support-vector-machine-introduction-to-machine-learning-algorithms-934a444fca47) | _Texto introductorio a los algoritmos de SVM. Tiene buenas ilustraciones._ |

### Sesgo y Varianza

| Link | Descripción |
| --- | --- |
| [Understanding the Bias-Variance Tradeoff](https://towardsdatascience.com/understanding-the-bias-variance-tradeoff-165e6942b229) | _Breve descripción de ambos conceptos._ |
| [Everything You Need To Know About Bias And Variance](https://www.simplilearn.com/tutorials/machine-learning-tutorial/bias-and-variance) | _Otro recurso interesante. Contiene un video que explica de forma clara el concepto de error y su relación con el sesgo y la varianza._ |

### Métodos de ensamble: Bagging, Boosting, Stacking

| Link | Descripción |
| --- | --- |
| [Ensemble methods: bagging, boosting and stacking](https://towardsdatascience.com/ensemble-methods-bagging-boosting-and-stacking-c9214a10a205) | _Introducción clara y concisa a los trés métodos de ensamble._ |
| [Bagging and Random Forest Ensemble Algorithms for Machine Learning](https://machinelearningmastery.com/bagging-and-random-forest-ensemble-algorithms-for-machine-learning/) | _Recurso que describe el uso de bagging en el ámbito de los algoritmos de Random Forest._ |


## Notebooks